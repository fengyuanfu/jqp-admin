# 关于低代码平台,基本上不再维护,有问题自己研究,也可以付费咨询,具体请看官网介绍

## 求职:

### 武汉,黄冈,九江,南昌等周边的工作,要求月薪15k

### 93年,黄冈市黄梅县

### 11年java开发,熟悉技术linux,html/js/css/react/vue/android/mysql/oracle/ssh/springboot,不排斥新技术,有需要就学习

### 微信hyz792901324,手机号18301535514

### 加我备注是提供工作还是咨询低代码项目问题

# jqp-admin低代码开发平台

## 所有代码全部开源

## 在线演示

https://www.jqp-admin.com
账号 admin
密码 1

登录后选择企业2,企业2目前配置了全量的权限

## 官网地址

https://jqp-admin.com

## 数据库以及markdown文档地址

https://gitee.com/hyz79/jqp-doc

## 前后端不分离版本(兼容分离和不分离版本)

https://gitee.com/hyz79/jqp-admin

## 前后端分离版本-前端

https://gitee.com/hyz79/jqp-admin-page

## 前后端分离版本-后端 (此项目长时间未更新,废弃,jqp-admin项目兼容分离和不分离版本)

https://gitee.com/hyz79/jqp-admin-rest
